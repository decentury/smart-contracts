'use strict';

var Token = artifacts.require("./Token.sol");

contract('Token', function(accounts) {

  let token;

  beforeEach(async function() {
    token = await Token.new(accounts[1]);
  });

//-----------------mint--------------------

  it('should start with a totalSupply of 10 ** 9 + 10 * 10 ** 6', async function() {
    let totalSupply = await token.totalSupply();

    assert.equal(totalSupply, 10 ** 9 + 10 * 10 ** 6);
  });

  it('should return isStarted true after construction', async function() {
    let isStarted = await token.isStarted();

    assert.equal(isStarted, true);
  });

  it('started balances should be correct', async function() {
    let balance0 = await token.balanceOf(accounts[0]);
    let balance1 = await token.balanceOf(accounts[1]);

    assert.equal(balance0, 10 ** 9);
    assert.equal(balance1, 10 * 10 ** 6);
  });
});
