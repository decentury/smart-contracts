pragma solidity ^0.4.15;

import './ERC20.sol';
import '../lib/Owned.sol';


contract Token is Owned, StandartToken {
    string public name = "Token";
    string public symbol = "TKN";
    uint public decimals = 0;

    function Token(address company) public {
        mint(msg.sender, 10 ** 9);
        mint(company, 10 * 10 ** 6);
        start();
    }

    function () public {
        revert();
    }

    function mint(address _to, uint256 _amount)
        internal
        returns (bool)
    {
        totalSupply = totalSupply.add(_amount);
        balances[_to] = balances[_to].add(_amount);
        return true;
    }

    function start()
        internal
        returns (bool)
    {
        isStarted = true;
        return true;
    }
}
